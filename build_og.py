import re

from pathlib import Path
from PIL import Image, ImageDraw, ImageFont

import urllib.request
import tqdm
from tqdm.contrib.concurrent import thread_map, process_map

def font(path, name, size):
    font_path = f"/tmp/{name}.ttf"
    urllib.request.urlretrieve(f"{path}/{name}.ttf", font_path)
    return ImageFont.truetype(font_path, size, encoding="unic")

def draw_text(image, text: str, style):
    draw = ImageDraw.Draw(image)

    x, y = style["axis"]

    if "letter_spacing" in style:
        for char in text:
            draw.text((x, y), char, fill=style["color"], font=style["font"])
            char_width = style["font"].getlength(char)
            x += char_width + style["letter_spacing"]
        
        return (x, style["font"].getlength(char))
    else:
        draw.text(style["axis"], text, fill=style["color"], font=style["font"])
    
    left, top, right, bottom = style["font"].getbbox(text)
    return (x+right, y+(bottom*len(text.split('\n'))))

def draw_letra(image, letra: str, style, posicao_inicial=(27, 67), espacamento=6):
    x, y = posicao_inicial

    for parte in re.split(r"(?:\n *\n)|^(?: ?\- )|^(?: ?\* )|^(?:\d+\. )", letra)[:3]:
        style_identifier = "verso"

        if parte.startswith("**"):
            style_identifier = "refrao"
            parte = re.sub(r'\*\*', '', parte)

        parte = re.sub(r'^ +', '', parte, flags=re.MULTILINE)
        parte = re.sub(r'^(( ?\- )|( ?\* )|(\d+\. ))', '', parte, flags=re.MULTILINE)
        # parte = re.sub(r'^\(.*)$\n', '', parte, flags=re.MULTILINE)

        text_style = {
            **style[style_identifier],
            **{"axis": (x, y)}
        }
        _, y = draw_text(image, parte, text_style)
        y += espacamento

width = 400
height = 210

style = {
    "titulo": {
        "font": font("https://github.com/ateliertriay/bricolage/raw/main/fonts/ttf", "BricolageGrotesque-SemiBold", 43),
        "color": "black",
        "axis": (75, 5),
        "letter_spacing": -3
    },
    "verso": {
        "font": font("https://github.com/ateliertriay/bricolage/raw/main/fonts/ttf", "BricolageGrotesque-Regular", 20),
        "color": "#374151",
    },
    "refrao": {
        "font": font("https://github.com/ateliertriay/bricolage/raw/main/fonts/ttf", "BricolageGrotesque-SemiBold", 20),
        "color": "black",
    }
}

icon = Image.open('public/icone-og.webp')
icon.thumbnail((40, 40), Image.Resampling.LANCZOS)

def draw_from_file(path):
    text = path.read_text()

    titulo = re.search(r'^Título: "(.*)"$', text, re.MULTILINE).group(1)
    letra = text.split('---')[2].strip()


    image = Image.new('RGB', (width, height), color="white")

    draw_text(image, titulo, style["titulo"])
    draw_letra(image, letra, style)

    draw = ImageDraw.Draw(image)
    image.paste(icon, (27, 11), mask=icon)

    output_image_path = f"dist/og/musicas/{path.stem}.webp"
    image.save(output_image_path, "webp", quality=90)

paths = list(Path('src/content/musicas/').glob("*.md"))

# for path in tqdm.tqdm(paths):
#     draw_from_file(path)

# thread_map(draw_from_file, paths)

process_map(draw_from_file, paths, chunksize=10)

