export const sanitizeHTML = (str: string|undefined) => {
	return str?.replaceAll('<', "&lt;")
    .replaceAll('>', "&gt;")
    .replaceAll('>', "&amp;") ?? '';
};
