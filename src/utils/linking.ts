import { config } from "./config";

/**
 * https://stackoverflow.com/a/111545
 */
export function encodeQueryData(data: {[key: string]: string}) {
  const ret: string[] = [];
  for (let d in data) {
    ret.push(`${encodeURIComponent(d)}=${encodeURIComponent(data[d])}`);
  }

  return ret.join('&');
}


export function androidLink() {
  return `intent://${location.href.replace("https://", '')}#Intent;scheme=canta;package=com.cantaigreja;S.browser_fallback_url=${encodeURIComponent(config.download.android)};end`;
}