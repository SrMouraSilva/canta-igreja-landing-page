let appProcess: any = undefined;
if (typeof process === 'undefined') {
    appProcess = {
        env: {
            VERCEL_GIT_COMMIT_REF: window.location.hostname === 'canta.app' ? 'main' : 'preview'
        }
    }
} else {
    appProcess = process;
}


const subdomain = appProcess?.env?.VERCEL_GIT_COMMIT_REF === 'main' ? '' : 'preview.';
const api = appProcess?.env?.VERCEL_GIT_COMMIT_REF === 'main' ? 'api' : 'api-preview';

export const config = {
    download: {
        android: 'https://play.google.com/store/apps/details?id=com.cantaigreja&hl=pt&gl=pt',
        ios: undefined,
    },
    site: `https://${subdomain}canta.app`,
    api: `https://${api}.canta.app/v1`,
    // api: `http://localhost:5000/v1`,
    og: {
        build: {
            enabled: appProcess.env.VERCEL_GIT_COMMIT_REF === undefined,
        },
        format: "webp",
        image: (site: URL|undefined, pathname: string) => new URL(`og${pathname.replace(/\/$/, "")}.${config.og.format}`, site).toString()
    }
}