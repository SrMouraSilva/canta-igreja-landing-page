export interface ItemRepertorioCompartilhado {
  id_musica?: number;
  termo?: string;
  tonalidade?: string;
  momento?: string;
}

export interface RepertorioCompartilhado {
  titulo: string,
  slug: string;

  conta: {
    nome: string;
    slug: string;
  }

  itens: ItemRepertorioCompartilhado[];

  criado_em: string;
  atualizado_em?: string;
};


export interface RepertorioQuery {
  titulo: string;
  slug: string;

  itens: {
    /**
     * Id da música
     */
    id?: number;
    termo?: string;

    momento?: string;
    /**
     * Tonalidade
     */
    tom?: string;
  }[];
}