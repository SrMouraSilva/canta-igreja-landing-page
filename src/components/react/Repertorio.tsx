import { Api, ApiException } from '@utils/Api';
import { Conversor } from '@utils/Conversor';
import { LetraUtil } from '@utils/LetraUtil';
import { androidLink } from '@utils/linking';
import type { Musica } from '@utils/model/Musica';
import type { ItemRepertorioCompartilhado, RepertorioCompartilhado } from '@utils/model/Repertorio';
import { sanitizeHTML } from '@utils/sanitize';
import { UserAgentUtil } from '@utils/UserAgentUtil';
import { Children, type FC, type ReactElement, useEffect, useState } from 'react';


export const Repertorio: FC = () => {
  const [repertorio, setRepertorio] = useState<RepertorioCompartilhado|undefined>();
  const [erro, setErro] = useState<ApiException|undefined>();

  useEffect(() => {
    const fetchRepertorio = async () => {
      try {
        const repertorio = await new Api().getRepertorio(window.location.search);

        setRepertorio(repertorio);
        document.title = `${sanitizeHTML(repertorio.titulo)} - Repertório - Canta Igreja`;
      } catch (erro) {
        setErro(erro);
      }
    }

    fetchRepertorio();
  }, [setRepertorio]);

  if (erro) {
    return <div className="min-h-[calc(80vh-16rem)] flex items-center justify-center" data-pagefind-ignore>
      <div className="mt-16 text-center">
        <h1 className="text-4xl lg:text-5xl font-bold lg:tracking-tight">
          {erro.status}{' '}{erro.message}
        </h1>
      </div>
    </div>
  }

  return <>
    <div id="repertorio-container"
      className="lyric-text mx-auto max-w-3xl mt-4"
      data-pagefind-ignore>
      <span className="text-blue-400 uppercase tracking-wider text-sm font-medium">
        Repertório compartilhado
      </span>
      {repertorio && <CabecalhoRepertorio repertorio={repertorio} />}

      {repertorio ? <ConteudoRepertorio repertorio={repertorio} /> : <div>Carregando...</div>}

      <div className="mt-8 prose" data-pagefind-ignore>A seleção de músicas e textos presentes são de total responsabilidade do usuário que criou e compartilhou o repertório.</div>
    </div>
  </>
}

const CabecalhoRepertorio: FC<{repertorio: RepertorioCompartilhado}> = ({repertorio}) => {
  const abrirAplicativoLink = androidLink();
  const repertorioQuery = new Conversor().asQuery(repertorio);

  const exibirBotoes = window.location.search.startsWith("?");
  const exibirFolheto = exibirBotoes;
  const exibirBotaoAbrirAplicativo = exibirBotoes && UserAgentUtil.isAndroid() && UserAgentUtil.isChrome();

  return <>
    <div className="lyric-header">
      <h1
        className="text-4xl lg:text-5xl font-bold lg:tracking-tight mt-1 lg:leading-tight mb-0">
        <span>{repertorio.titulo}</span>
      </h1>
      <span className="text-slate-500">
        Criado por <span className="text-slate-700">{sanitizeHTML(repertorio.conta.nome)}</span>
      </span>
      <span className="text-slate-500">
        {' '} em {new Date(repertorio.criado_em).toLocaleString("pt-BR")}
        {repertorio.atualizado_em && <> e atualizado em {new Date(repertorio.atualizado_em).toLocaleString("pt-BR")}</>}
      </span>
    </div>
    
    <div className="flex flex-row-reverse">
      <span className="flex">
        {exibirBotaoAbrirAplicativo && <a
          href={abrirAplicativoLink}
          className="
            border-2 border-blue-500 hover:bg-slate-100 focus:bg-slate-100 text-blue-500 hover:text-blue-600 focus:text-blue-600
            font-bold py-2 px-4 rounded cursor-pointer
            flex items-center align-end
            mt-2 mr-2 md:mt-0
          ">
          Abrir no app
        </a>
        }
        {exibirFolheto && <a id="folheto"
          href={`https://livreto.canta.app/?repertorio=${repertorioQuery}`}
          target="_blank"
          className="
            border-2 border-blue-500 hover:bg-slate-100 focus:bg-slate-100 text-blue-500 hover:text-blue-600 focus:text-blue-600
            font-bold py-2 px-4 rounded cursor-pointer
            flex items-center align-end
            mt-2 md:mt-0
          ">
          <svg aria-hidden className="mr-2" width="18" height="15" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 9L9 13M9 13L5 9M9 13V1" stroke="#3b82f6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M1 17V18C1 18.7956 1.31607 19.5587 1.87868 20.1213C2.44129 20.6839 3.20435 21 4 21H14C14.7956 21 15.5587 20.6839 16.1213 20.1213C16.6839 19.5587 17 18.7956 17 18V17" stroke="#3b82f6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
          Gerar Folheto
        </a>}
      </span>
    </div>
  </>
}


const ConteudoRepertorio: FC<{repertorio: RepertorioCompartilhado}> = ({repertorio}) => {
  const [musicas, setMusicas] = useState<{[key: number]: Musica;}|undefined>();

  useEffect(() => {
    const fetchItensRepertorio = async () => {
      const musicas = await new Api().getMusicas(repertorio);
      setMusicas(musicas);
    }

    fetchItensRepertorio();
  }, [setMusicas]);

  return <div className="text-lg leading-6 md:mt-6">
    <ul id="repertorio-conteudo" className="divide-y divide-gray-200">
      {musicas === undefined
        ? <>Carregando...</>
        : repertorio.itens.map(item => <ItemRepertorio item={item} musica={musicas[item.id_musica ?? -1]} />)}
    </ul>
  </div>
}

const ItemRepertorio: FC<{item: ItemRepertorioCompartilhado, musica?: Musica}> = ({item, musica}) => {
  const hasMomentoAndTonalidade = item.momento && item.tonalidade;

  const titulo = musica
    ? sanitizeHTML(musica.titulo)
    : sanitizeHTML(item.termo)

  const letra = musica
    ? sanitizeHTML(LetraUtil.limparLetra(musica.letra))
    : '(música incluída manualmente pelo usuário)';

  const MaybeLink: FC<{children: ReactElement}> = ({children}) => item.id_musica
    ? <a href={`/musicas/${item.id_musica}`}>{children}</a>
    : children;

  return <li className="first:rounded-t-lg last:rounded-b-lg hover:bg-slate-100 focus:bg-slate-100 border-solid border-slate-200">
    <MaybeLink>
      <span className="block no-underline py-3 md:p-3">
        {item.momento && <span className="uppercase text-slate-500">{sanitizeHTML(item.momento)}</span>}
        {hasMomentoAndTonalidade && <span className="text-slate-500"> - </span>}
        {item.tonalidade && <span className="uppercase text-slate-500">{sanitizeHTML(item.tonalidade)}</span>}

        <div className="font-semibold leading-7">{titulo}</div>
        <div className={`text-slate-500 font-normal line-clamp-2 ${musica ? '' : 'italic'}`}>
          {letra}
        </div>
      </span>
    </MaybeLink>
  </li>
};
