current_path=$(pwd)

# git clone \
git clone -n --depth=1 --filter=tree:0 \
  https://gitlab.com/SrMouraSilva/canta-igreja-api.git \
  /tmp/lyrics
cd /tmp/lyrics
git sparse-checkout set --no-cone dados/musicas
git checkout

for filename in /tmp/lyrics/dados/musicas/*.txt; do
    # Add --- between headers
    sed -i '1s/^/---\n/' $filename
    sed -i '0,/^\s*$/s//---\n/' $filename
    # Escape "
    sed -i '/^Título:/ s/"/\\"/g' $filename
    # Envolve ""
    sed -i 's/^Título: \(.*\)$/Título: "\1"/' $filename
    
    file=$(basename ${filename})
    cp $filename "$current_path/src/content/musicas/${file%.txt}.md"
done

cd $current_path

npm run build

# Generate images
if [ -n "${CI}" ]; then
  pip3 install tqdm pillow

  mkdir -p dist/og/musicas
  python3 build_og.py
fi