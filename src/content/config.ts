// 1. Import utilities from `astro:content`
import { z, defineCollection } from 'astro:content';

// 2. Define your collection(s)
const blogCollection = defineCollection({
  schema: z.object({
    draft: z.boolean(),
    title: z.string(),
    snippet: z.string(),
    image: z.object({
      src: z.string(),
      alt: z.string(),
    }),
    publishDate: z.string().transform(str => new Date(str)),
    author: z.string().default('Astroship'),
    category: z.string(),
    tags: z.array(z.string()),
  }),
});

const zNullableList = z.string().nullable().transform(it => it ? it.split("; ") : [])

const musicaCollection = defineCollection({
  schema: z.object({
    Índice: z.number(),
    Título: z.string(),
    Rótulos: zNullableList,
    Compositores: zNullableList,
    Letristas: zNullableList,
    Musicistas: zNullableList,
    YouTube: zNullableList,
    Partitura: z.string().nullable(),
    CifraClub: z.string().nullable(),
    Tonalidade: z.string().nullable().optional(),
  }),
});

// 3. Export a single `collections` object to register your collection(s)
//    This key should match your collection directory name in "src/content"
export const collections = {
  'blog': blogCollection,
  'musicas': musicaCollection
};