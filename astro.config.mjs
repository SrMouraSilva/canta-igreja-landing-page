import mdx from "@astrojs/mdx";
import react from '@astrojs/react';
import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import icon from "astro-icon";
import { defineConfig } from "astro/config";
import { config } from "./src/utils/config";


// https://astro.build/config
export default defineConfig({
  site: config.site,
  integrations: [
    tailwind({
      applyBaseStyles: false
    }),
    mdx(),
    sitemap(),
    icon(),
    react({
      include: ['**/react/*'],
    }),
  ],
  redirects: {
    '/android': config.download.android
  }
});
