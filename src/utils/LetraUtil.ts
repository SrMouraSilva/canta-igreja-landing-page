export class LetraUtil {
  static limparLetra(letra: string) {
    return letra
      .replace(/(_|\*|\\)/gi, '')
      .replace(/(\s+)/gi, ' ')
      .trim();
  }
}
