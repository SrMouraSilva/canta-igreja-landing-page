import { Base64Unicode } from "./Base64Unicode";
import type { RepertorioCompartilhado, RepertorioQuery } from "./model/Repertorio";

export class Conversor {

  asQuery(repertorio: RepertorioCompartilhado): string {
    const query: RepertorioQuery = {
      titulo: repertorio.titulo,
      slug: repertorio.slug,

      itens: repertorio.itens
        .map(it => ({id: it.id_musica, tom: it.tonalidade, momento: it.momento}))
        .filter(it => it.id !== undefined),
    };

    return Base64Unicode.encode(query);
  }
}
