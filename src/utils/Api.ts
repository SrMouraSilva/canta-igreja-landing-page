import type { RepertorioCompartilhado } from "@utils/model/Repertorio";
import { config } from "./config";
import type { Musica } from "./model/Musica";
import { BatchFetch } from "./BatchFetch";

export class ApiException extends Error {
  constructor(public status: number, mensagem: string) {
    super(mensagem);
  }
}

export class NotFoundException extends ApiException {
  constructor(mensagem: string) {
    super(404, mensagem);
  }
}


export class Api {
  static readonly repertorioPadrao: RepertorioCompartilhado = {
    titulo: "Repertório exemplo",
    slug: "exemplo",
  
    conta: {
      nome: "Exemplo",
      slug: "exemplo"
    },
    itens: [{id_musica: 1}, {id_musica: 2}, {id_musica: 3}],
    criado_em: new Date().toISOString()
  };

  async getRepertorio(locationSearch: string): Promise<RepertorioCompartilhado> {
    const path = decodeURIComponent(locationSearch);

    if (!path.startsWith("?")) {
      return Api.repertorioPadrao;
    }

    if (path.split(':').length != 2) {
      throw new NotFoundException("Repertório não encontrado")
    }

    const [conta, repertorio] = path.substring(1).replaceAll('=', '').split(":", 2);

    const response = await fetch(`${config.api}/repertorios/${conta}/${repertorio}`)
      .catch(e => {
        console.error(e);
        throw new ApiException(0, "Não foi possível carregar o repertório solicitado")
      })

    if (response.status >= 400) {
      if (response.status == 404) {
        throw new NotFoundException("Repertório não encontrado")
      } else {
        throw new ApiException(response.status, "Não foi possível carregar o repertório solicitado")
      }
    }

    return await response.json();
  }

  async getMusicas(repertorio: RepertorioCompartilhado) {
    const urls = repertorio.itens
      .filter(item => item.id_musica)
      .map(item => `https://api.canta.app/v1/musicas/${item.id_musica}`);

    const response = await BatchFetch.fetch(urls, 5);
    const itens: {[key: number]: Musica} = {}
    response.forEach(it => { itens[it.id_musica] = it });

    return itens;
  }
}