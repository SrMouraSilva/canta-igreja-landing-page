export class UserAgentUtil {
  static readonly isiOS = () => navigator
    ? (navigator.userAgent.match('iPad') || navigator.userAgent.match('iPhone') || navigator.userAgent.match('iPod'))
    : false;
  static readonly isAndroid = () => navigator?.userAgent.match('Android') ?? false;

  static readonly isMobile = () => UserAgentUtil.isAndroid() || UserAgentUtil.isiOS();

  static isChrome = () => (navigator.userAgent.match("Android")?.length ?? 0) >= 1 && !!window.chrome;
}