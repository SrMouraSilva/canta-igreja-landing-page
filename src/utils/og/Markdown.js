import markdownit from 'markdown-it';


export class Markdown {

  static md = markdownit().disable([
    // Texto
    //'balance_pairs', 'emphasis', // Negrito e itálico
    //'list'
    // 'heading',
    // 'lheading',
    'table',
    'fence',
    'code',
    'html_block',
    'html_inline',
    'hr',
    'reference',
    'blockquote',
    'escape',
    'backticks',
    'link',
    'image',
    'autolink',
    'strikethrough',
    'backticks',
    // 'hardbreak', 'softbreak'

    'normalize',
    'linkify',
    'replacements',
    'smartquotes',
  ]);

  static render(content) {
    return this.md.render(content);
  }
}