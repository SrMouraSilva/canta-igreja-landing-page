---
Índice: 1
Título: "Hosana"
Rótulos: 
Compositores: D. R.
Letristas: 
Musicistas: 
YouTube: https://youtu.be/tWkH7q-V_Ak
Partitura: 
CifraClub: 
Tonalidade: D
---

**Hosana, Hosana ao Rei.** (2x)

1. Mantos e palmas espalhando vai
   o povo alegre de Jerusalém.
   Lá bem ao longe se começa a ver
   o Filho de Deus que montado vem.

**Enquanto mil vozes ressoam por aí:
Hosana ao que vem em nome do Senhor
Com um alento de grande exclamação
prorrompem com voz triunfal!**

**Hosana, Hosana ao Rei.** (2x)

2. Como na estrada de Jerusalém
   um dia também poderemos cantar
   A Jesus Cristo que virá outra vez
   para levar nos ao eterno lar
